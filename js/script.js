/**#Теоретический вопрос
 *
 * setTimeout() - функция, которая выполняется только единожды через указанный промежуток времени (в милисекундах)
 * setInterval() - функция, которая выполняется многократно через указанный промежуток времени.
 * Если передать в  setTimeout() нулевую задержку, она сработает сразу после выполнения предыдущего кода.
 * Это своего рода планирование задач - заданная таким образом функция помещается в стэк вызовов, но ждет выполнения
 * предшествующего кода, и только после этого выполняется.
 * clearInterval() - функция удаления интервала, которая нужна для очистки стэка. Иначе, даже после закрытия страницы,
 * колбэк из setInterval будет выполняться бесконечно.

 * */
const wrapper = document.querySelector('.images-wrapper');
const images = wrapper.querySelectorAll('img');
const sectionBtns = document.querySelector('.btns');
const btnStop = document.createElement('button');
const btnContinue = document.createElement('button');

let timeout = 10;
let prevImg = null;
let index = 1;
let timer = setInterval(showImg, timeout * 1000);

window.addEventListener('load', () => {
    images[0].style.display = 'block';
    prevImg = images[0];

    btnStop.innerText = 'Stop show';
    sectionBtns.appendChild(btnStop);

    btnContinue.innerText = 'Continue show';
    sectionBtns.appendChild(btnContinue);
});

btnStop.addEventListener('click', () => {
    clearInterval(timer);
});

btnContinue.addEventListener('click', () => {
    timer = setInterval(showImg, timeout * 1000);
});

function showImg() {
    if (prevImg) {
        $(prevImg).fadeOut(500);
    }
    if (index === images.length) {
        index = 0;
    }
    setTimeout(() => {
        $(images[index]).fadeIn(500);
        prevImg = images[index];
        index++;
    }, 500);
}




